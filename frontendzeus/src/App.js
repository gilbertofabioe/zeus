import MainComponent from './components/maincomponent';
import './App.css';
import {useState, useEffect} from 'react'
import {BsTrash, BsBookmarkCheck, BsBookmarkCheckFill} from 'react-icons/bs'
import {FiEdit3} from 'react-icons/fi' 

//const API = "http://localhost:5000";
const API = "http://localhost:5001";
const DataBase = "/gastos";

function App() {

  const [title, setTitle] = useState('');
  const [valor, setValor] = useState();
  const [banho, setBanho] = useState(false);
  const [vet, setVet] = useState(false);
  const [comida, setComida] = useState(false);
  const [todos, setTodos] = useState([]);
  const [loading, setLoaing] = useState(false);
  const [editing, setEditing] = useState(null);
  const [editValue, setEditValue] = useState({});

  //Carregar gastos no pageload
  useEffect(()=>{
    const loadData = async() =>{
      setLoaing(true);

      const res = await fetch(API + "/gastos")
      .then((res)=> res.json())
      .then((data)=>data)
      .catch((err)=>console.log(err))

      setLoaing(false);
      setTodos(res);
    };
    loadData();
  },[])

  const handleSubmit = async (e)=>{
    e.preventDefault();

    const valorFormat = parseFloat(valor).toFixed(2)
    const date = new Date().toLocaleDateString();
 
    const todo = {
      //id: Math.floor(Math.random()*500),
      date,
      title,
      valorFormat,
      banho,
      vet,
      comida,
    };
    
    //enviar para DB
    console.log(todo)
    await fetch(API + DataBase,{
      method: "POST",
      body: JSON.stringify(todo),
      headers:{
        "Content-Type": "application/json",
      },
    });


    /*axios.post(`${API}${DataBase}`,todo).then((res)=>{
      console.log("Enviado com sucesso")

      setTitle("")
      setValor()
      setTodos((prevState)=>[...prevState, todo]);
    }).catch((err)=>{
      console.log(err)
    })*/

    setTitle("")
    setValor()


    setTodos((prevState)=>[...prevState, todo]);
        
  };

  const handleDelete = async (id)=>{

    await fetch(API + DataBase + "/"+id,{
      method: "DELETE"
    }); 

    setTodos((prevState) => prevState.filter((todo)=>todo._id!==id));

  }

  const handleEdit = async (e)=>{
    e.preventDefault();
    const todo = editValue;

    if(valor){
      const valorFormat = parseFloat(valor).toFixed(2);
      todo.valorFormat = valorFormat
    }else{
      const valorFormat = parseFloat(todo.valorFormat).toFixed(2);
      todo.valorFormat = valorFormat
    }
    if(title!==""){
      todo.title = title
    }

    //enviar para DB
    const data = await fetch(API + DataBase + "/"+todo._id,{
      method: "PATCH",
      body:JSON.stringify(todo),
      headers: {
        "Content-Type": "application/json",
      },
    }); 

    setTodos((prevState) => prevState.map((t)=>(t._id === data._id ? (t=data): t)));

    setEditValue({})

    handleCancel()
        
  };

  // const handleEdit = async (todo)=>{

  //   //e.preventDefault();
  //     todo.valorFormat = 0
  //     todo.title = "Novo Valor"

  //   const data = await fetch(API + DataBase + "/311",{
  //     method: "PUT",
  //     body:JSON.stringify(todo),
  //     headers: {
  //       "Content-Type": "application/json",
  //     },
  //   }); 

  //   setTodos((prevState) => prevState.map((t)=>(t.id === data.id ? (t=data): t)));
  //   handleCancel()
  // }

  const handleEditing = (todo)=>{
    
    if(editing===null){
      setEditing(todo)
    }else{
      handleCancel()
    }
    
  }

  const handleCancel = ()=>{

    setEditing(null)
    setTitle("")
    setValor()
  }

  if(loading){
    return <p>Carregando...</p>
  }

  return (
    <div className="App">
      <div className='todo-header'>
        <h1>Projeto Zeus</h1>
      </div>
      <div className='form-todo'>
        <h2>Insira sua próxima compra:</h2>
        <div className='form'>
          <form onSubmit={handleSubmit}>
            <div className='form-control'>
              <label htmlFor='title'>Qual o seu gasto?</label>
              <input 
                type="text" 
                name="title" 
                placeholder="Informe o gasto" 
                onChange={(e) => setTitle(e.target.value)}
                value={title || ""}
                required 
              />
            </div>
            <div className='form-control'>
              <label htmlFor='valor'>Qual o valor do seu gasto?</label>
              <input 
                type="number" 
                name="valor" 
                placeholder="Informe o valor do gasto" 
                onChange={(e) => setValor(e.target.value)}
                value={valor || ""}
                required 
              />
            </div>
            <div className='form-control'>
              <label htmlFor='Banho'>Houve banho?</label>
              <input 
                type="checkbox"
                className='checkmark' 
                name="banho"
                onChange={(e) => {
                  if(e.target.checked){
                    setBanho(true)
                  }else{
                    setBanho(false)
                  }
                }}
              />
            </div>
            <div className='form-control'>
              <label htmlFor='Vet'>Houve consulta no veterinário?</label>
              <input 
                type="checkbox" 
                className='checkmark'
                name="vet"
                onChange={(e) => {
                  if(e.target.checked){
                    setVet(true)
                  }else{
                    setVet(false)
                  }
                }}
              />
            </div>
            <div className='form-control'>
              <label htmlFor='Vet'>Houve compra de comida?</label>
              <input 
                type="checkbox" 
                className='checkmark'
                name="comida"
                onChange={(e) => {
                  if(e.target.checked){
                    setComida(true)
                  }else{
                    setComida(false)
                  }
                }}
              />
            </div>
            <input type="submit" value="Adicionar Gasto" />
          </form>
        </div>
      </div>
      <div className='list-todo'>
        <h2>Lista de gastos:</h2>
        {todos.length ===0 && <p>Não há tarefas</p>}
        {todos.map((todo)=>(
          <div className='todo' key={todo._id}>
            <h3>{todo.title}</h3>
            <p>Data: {todo.date}</p>
            <p>R$: {todo.valorFormat}</p>
            <div className='actions'>
              <span title='Banho'>Banho: 
                {!todo.banho ? <BsBookmarkCheck />:<BsBookmarkCheckFill />}
              </span>
              <span title='Vet'> Vet: 
                {!todo.vet ? <BsBookmarkCheck />:<BsBookmarkCheckFill />}
              </span>
              <span title='Comida'> Comida: 
                {!todo.comida ? <BsBookmarkCheck />:<BsBookmarkCheckFill />}
              </span> 
              <br/>
              <br/>
              <BsTrash onClick={()=>handleDelete(todo._id)}/>
              <FiEdit3 onClick={()=>handleEditing(todo)}/>
              <div className='editing'>
                {editing === todo ? (
                <div className='form'>
                  <form onSubmit={handleEdit}>
                    <div className='form-control'>
                      <label htmlFor='title'>Gasto:</label>
                      <input 
                        type="text" 
                        name="title" 
                        onChange={(e) => setTitle(e.target.value)}
                        value={title || todo.title}
                      />
                    </div>
                    <div className='form-control'>
                      <label htmlFor='valor'>Valor do gasto:</label>
                      <input 
                        type="number" 
                        name="valor" 
                        onChange={(e) => setValor(e.target.value)}
                        value={valor || todo.valorFormat}
                      />
                    </div>
                    <button onClick={()=>{
                      setEditValue(editing)
                      console.log(editValue)
                    }} >Salvar</button>
                    <button onClick={handleCancel} >Cancelar</button>
                  </form>
              </div>
              ): <></> }
              </div>

            </div>
          </div>
        ))}
      </div>
    </div>
  );
}

export default App;

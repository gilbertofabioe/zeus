const router = require('express').Router()
const Gastos = require('../models/Gastos')

//criar

router.post('/', async (req,res)=>{

    const {date, title, valorFormat, banho, vet, comida} = req.body
    //const data = new Date().toLocaleDateString();

    if(!valorFormat){
        res.status(422).json({error: 'Valor é obrigatório'})
        return
    }

    const gastos = {
        date,
        title,
        valorFormat,
        banho,
        vet,
        comida,
    }

    try{

        await Gastos.create(gastos)

        res.status(201).json({msg: `Gasto inserido com sucesso`})

    }catch(error){
        res.status(500).json({error: error})
    }

})

//ler
router.get('/', async (req,res)=>{
    try{
        
        const gastos = await Gastos.find()

        res.status(200). json(gastos)
        
    }catch(error){
        res.status(500).json({error: error})
    }
})

/*router.get('/:id', async (req,res)=>{
    const id = req.params.id
    try{
        
        const people = await Person.findOne({ _id: id})

        res.status(200). json(people)
        
    }catch(error){
        res.status(500).json({error: error})
    }
})*/

router.get('/:id', async (req,res)=>{
    const id = req.params.id

    try{
        
        const gastos = await Gastos.findOne({ _id: id})

        if(!gastos){
            res.status(422).json({error: 'Gasto não encontrado'})
            return
        }

        res.status(200).json(gastos)
        
    }catch(error){
        res.status(500).json({error: error})
    }
})

router.patch('/:id', async (req,res)=>{

    const id = req.params.id

    const {date, title, valorFormat, banho, vet, comida} = req.body

    const gastos = {
        date,
        title,
        valorFormat,
        banho,
        vet,
        comida,
    }
    
    try{

        const updatedGasto = await Gastos.updateOne({_id: id}, gastos)

        if(updatedGasto.matchedCount === 0){
            res.status(422).json({error: 'Gasto não encontrado'})
            return
        }

        res.status(200).json(gastos)

    }catch(error){
        res.status(500).json({error: error})
    }
})

router.delete('/:id', async (req,res)=>{
    
    const id = req.params.id

    const gastos = await Gastos.findOne({ _id: id})

    if(!gastos){
        res.status(422).json({error: 'Gasto não encontrado'})
        return
    }

    try{

        await Gastos.deleteOne({ _id: id})

        res.status(200).json({message: `Gasto foi apagado com sucesso`})

    }catch(error){
        res.status(500).json({error: error})
    }


})

module.exports = router;